package udep.ing.poo.appcondb;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import udep.ing.poo.appcondb.db.Conexion;
import udep.ing.poo.appcondb.db.Usuario;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    EditText inputId, inputNombre, inputTelefono;
    Button botonRegistrar;
    ImageView imagen;
    Bitmap foto = null;

    Conexion conexion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        inputId = (EditText)findViewById(R.id.insertId);
        inputNombre = (EditText)findViewById(R.id.insertNombre);
        inputTelefono = (EditText)findViewById(R.id.insertTelefono);

        botonRegistrar = (Button)findViewById(R.id.button2);
        botonRegistrar.setOnClickListener(this);

        imagen = (ImageView)findViewById(R.id.imageView);
        imagen.setOnClickListener(this);

        conexion = new Conexion(this);
    }

    private static final int REQUEST_CODE_STORAGE_PERMISSION = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 2;

    @Override
    public void onClick(View view) {

        if (view.getId() == botonRegistrar.getId()) {

            String idUsuario = inputId.getText().toString();
            String nombre = inputNombre.getText().toString();
            String telefono = inputTelefono.getText().toString();

            if (!idUsuario.equals("") && !nombre.equals("") && !telefono.equals("")) {

                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_ID, idUsuario);
                values.put(Usuario.CAMPO_NOMBRE, nombre);
                values.put(Usuario.CAMPO_TELEFONO, telefono);

                if (foto != null) {
                    byte[] bytes; // bytes[] corresponde a un blob en la BD
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    this.foto.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    bytes = out.toByteArray();
                    values.put(Usuario.CAMPO_FOTO, bytes);
                }

                long insert = db.insert(Usuario.TABLA_USUARIOS, Usuario.CAMPO_ID, values);
                Toast.makeText(this, "id:" + insert, Toast.LENGTH_SHORT).show();

                if (insert>-1) {

                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);

                }

            }
        } else if (view.getId() == imagen.getId()) {

            /* verificar si la app tiene permiso para acceder a la memoria del celular */
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                /* solicitar el permiso */
                ActivityCompat.requestPermissions(MainActivity2.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE_STORAGE_PERMISSION);

                /* si ya tiene el permiso */
            } else {

                /* seleccionamos una imagen */
                seleccionarImagen();

            }
        }

    }

    private void seleccionarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent,REQUEST_CODE_SELECT_IMAGE);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        /* verificamos si la solicitud de permiso fue en el onclick */
        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION && grantResults.length>0) {

            /* si el usuario aceptó */
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                seleccionarImagen();
            } /* si el usuario negó */
            else {
                Toast.makeText(this,"Permiso denegado",Toast.LENGTH_SHORT).show();
            }

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {

            if (data!=null && data.getData()!=null) {

                try {

                    Uri selectedImageUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectedImageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(in);
                    imagen.setImageBitmap(bitmap);
                    this.foto = bitmap;

                } catch (FileNotFoundException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

        }
    }

    /*

    cuando abre la actividad: onCreate
    cuando clickeo en la imagen: onClick
        verifica permiso
            si no tiene permiso: pide permiso -> onRequestPermissionsResult -> selecciona Imagen
            si ya tiene permiso: selecciona Imagen
        luego de selecciona Imagen: onActivityResult

     */
}