package udep.ing.poo.appcondb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import udep.ing.poo.appcondb.db.Conexion;
import udep.ing.poo.appcondb.db.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    Conexion conexion;
    Button botonRegistrarUsuario;

    ArrayAdapter<String> adaptador;
    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conexion = new Conexion(this);

        botonRegistrarUsuario = (Button)findViewById(R.id.button);
        botonRegistrarUsuario.setOnClickListener(this);

        lista = (ListView)findViewById(R.id.listView1);
        adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, getLista());
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == botonRegistrarUsuario.getId()) {

            Intent i = new Intent(this, MainActivity2.class);
            startActivity(i);

        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Intent intent = new Intent(this, MainActivity3.class);

        Bundle extras = new Bundle();
        extras.putSerializable("usuario",listaUsuarios.get(i));

        intent.putExtras(extras);
        startActivity(intent);

    }

    List<Usuario> listaUsuarios;

    private List<String> getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        List<String> listaNombres = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Usuario.CAMPO_ID, Usuario.CAMPO_NOMBRE, Usuario.CAMPO_TELEFONO, Usuario.CAMPO_FOTO};

        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, null,null,null,null,Usuario.CAMPO_ID);
        Bitmap bitmap;

        while (cursor.moveToNext()) {

            bitmap = null;
            if (cursor.getBlob(3)!=null) {
                byte[] bytes = cursor.getBlob(3);
                bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            }

            Usuario u = new Usuario(cursor.getInt(0), cursor.getString(1), cursor.getString(2),bitmap);
            listaUsuarios.add(u);
            listaNombres.add(u.getNombre());

        }

        return listaNombres;
    }
}