package udep.ing.poo.appcondb;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import udep.ing.poo.appcondb.db.Conexion;
import udep.ing.poo.appcondb.db.Direccion;
import udep.ing.poo.appcondb.db.Usuario;

public class MainActivity3 extends AppCompatActivity implements View.OnClickListener {

    EditText editId, editNombre, editTelefono;
    Button botonEditar, botonEliminar, botonAgregarDireccion, botonSubirCV, botonBajarCV;
    Conexion conexion;

    TextView textView;
    ImageView imagen;
    Usuario u;

    byte[] blobImagen;
    byte[] blobCV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        editId = (EditText)findViewById(R.id.editId);
        editNombre = (EditText)findViewById(R.id.editNombre);
        editTelefono = (EditText)findViewById(R.id.editTelefono);

        Bundle extras = getIntent().getExtras();
        u = (Usuario)extras.getSerializable("usuario");

        editId.setText(u.getId()+"");
        editNombre.setText(u.getNombre());
        editTelefono.setText(u.getTelefono());

        botonEditar = (Button)findViewById(R.id.button4);
        botonEliminar = (Button)findViewById(R.id.button5);
        botonAgregarDireccion = (Button)findViewById(R.id.button3);
        botonSubirCV = (Button)findViewById(R.id.button7);
        botonBajarCV = (Button)findViewById(R.id.button8);

        botonEditar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonAgregarDireccion.setOnClickListener(this);
        botonSubirCV.setOnClickListener(this);
        botonBajarCV.setOnClickListener(this);

        editId.setEnabled(false);

        conexion = new Conexion(this);

        textView = (TextView)findViewById(R.id.textView);
        textView.setText("Direcciones:");

        imagen = (ImageView)findViewById(R.id.imageView2);
        imagen.setOnClickListener(this);

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_FOTO, Usuario.CAMPO_CV};
        String[] valoresWhere = {u.getId()+""};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS,campos,Usuario.CAMPO_ID+"=?",valoresWhere,null,null,null);
        cursor.moveToFirst();
        if (cursor.getBlob(0) != null) {
            this.blobImagen = cursor.getBlob(0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blobImagen,0, blobImagen.length);
            imagen.setImageBitmap(bitmap);
            u.setImagen(bitmap);
        }
        if (cursor.getBlob(1) == null) {
            botonBajarCV.setEnabled(false);
        } else {
            this.blobCV = cursor.getBlob(1);
        }

        leerDirecciones();
    }

    @Override
    public void onClick(View view) {

        SQLiteDatabase db = conexion.getWritableDatabase();
        String[] valoresWhere = {editId.getText().toString()};
        String where = Usuario.CAMPO_ID + "=?";

        if (view.getId() == botonEditar.getId()) {

            ContentValues values = new ContentValues();
            values.put(Usuario.CAMPO_NOMBRE, editNombre.getText().toString());
            values.put(Usuario.CAMPO_TELEFONO, editTelefono.getText().toString());

            if (blobCV != null) values.put(Usuario.CAMPO_CV, blobCV);

            int update = db.update(Usuario.TABLA_USUARIOS, values, where,valoresWhere);

            Toast.makeText(this, "actualizados: " + update, Toast.LENGTH_SHORT).show();

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);

        } else if (view.getId() == botonEliminar.getId()) {

            int delete = db.delete(Usuario.TABLA_USUARIOS, where,valoresWhere);
            Toast.makeText(this, "eliminados: " + delete, Toast.LENGTH_SHORT).show();

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);

        } else if (view.getId() == botonAgregarDireccion.getId()) {

            Intent i = new Intent(this,MainActivity4.class);

            Bundle extras = new Bundle();
            extras.putSerializable("usuario",u);
            i.putExtras(extras);

            startActivity(i);

        } else if (view.getId() == imagen.getId() && blobImagen !=null) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity3.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_IMAGE_STORAGE_PERMISSION);
            } else {
                guardarImagen();
            }

        } else if (view.getId() == botonSubirCV.getId()) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity3.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE_READ_STORAGE_PERMISSION);
            } else {
                seleccionarArchivo();
            }
        } else if (view.getId() == botonBajarCV.getId()) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity3.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_IMAGE_STORAGE_PERMISSION);
            } else {
                guardarCV();
            }
        }


    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_WRITE_IMAGE_STORAGE_PERMISSION && grantResults.length>0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                guardarImagen();
            }
            else {
                Toast.makeText(this,"Permiso denegado",Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == REQUEST_CODE_READ_STORAGE_PERMISSION && grantResults.length>0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                seleccionarArchivo();
            }
            else {
                Toast.makeText(this,"Permiso denegado",Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == REQUEST_CODE_WRITE_FILE_STORAGE_PERMISSION && grantResults.length>0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                guardarCV();
            } else {
                Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void seleccionarArchivo() {
        Intent seleccionar = new Intent(Intent.ACTION_GET_CONTENT);
        seleccionar.setType("*/*");
        seleccionar.addCategory(Intent.CATEGORY_DEFAULT);
        seleccionar = Intent.createChooser(seleccionar,"Elegir archivo");

        if (seleccionar.resolveActivity(getPackageManager())!=null) {
            startActivityForResult(seleccionar, REQUEST_CODE_READ_STORAGE_PERMISSION);
        }
    }

    private void guardarImagen() {
        guardarArchivo("fotoPerfil.jpg",blobImagen);
    }

    private void guardarCV() {
        guardarArchivo("archivoCV.jpg",blobCV);
    }

    private void guardarArchivo(String fileName, byte[] blob) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),fileName);

        try {

            if (!file.exists()) file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(blob);
            fos.close();
            Toast.makeText(this,"Se guardó en " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            Toast.makeText(this,e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private static int REQUEST_CODE_WRITE_IMAGE_STORAGE_PERMISSION = 1;
    private static int REQUEST_CODE_READ_STORAGE_PERMISSION = 2;
    private static int REQUEST_CODE_WRITE_FILE_STORAGE_PERMISSION = 3;

    public void leerDirecciones() {

        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] campos = {Direccion.CAMPO_ID, Direccion.CAMPO_CALLE, Direccion.CAMPO_NUMERO};
        String[] valoresWhere = {u.getId()+""};
        String where = Direccion.CAMPO_USUARIO_FK + "=?";

        Cursor cursor = db.query(Direccion.TABLA_DIRECCIONES, campos, where,valoresWhere,null,null,null);

        textView.setText("Direcciones: "+cursor.getCount()+"");

    }


    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_READ_STORAGE_PERMISSION && resultCode == RESULT_OK) {

            if (data!=null && data.getData()!=null) {

                try {

                    Uri selectedFileUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectedFileUri);
                    ByteArrayOutputStream os = new ByteArrayOutputStream();

                    int nRead;
                    blobCV = new byte[16384];

                    while ((nRead = in.read(blobCV,0,blobCV.length)) != -1) {
                        os.write(blobCV,0,nRead);
                    }

                    blobCV = os.toByteArray();

                    botonBajarCV.setEnabled(true);
                    Toast.makeText(this,"Se guardó el archivo", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {

                }

            }

        }
    }
}