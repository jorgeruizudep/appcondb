package udep.ing.poo.appcondb.db;

public class Direccion {

    private String calle;
    private int numero;
    private Usuario usuario;
    private int id;

    public Direccion(String calle, int numero, Usuario usuario, int id) {
        this.calle = calle;
        this.numero = numero;
        this.usuario = usuario;
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static final String TABLA_DIRECCIONES = "direcciones";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_CALLE = "calle";
    public static final String CAMPO_NUMERO = "numero";
    public static final String CAMPO_USUARIO_FK = "usuario_id";

    public static final String CREAR_TABLA_DIRECCIONES = "CREATE TABLE " + TABLA_DIRECCIONES + " (" +
            CAMPO_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            CAMPO_CALLE + " TEXT, " +
            CAMPO_NUMERO + " INTEGER, "+
            CAMPO_USUARIO_FK + " INTEGER, "+
            "FOREIGN KEY (" + CAMPO_USUARIO_FK + ") REFERENCES " + Usuario.TABLA_USUARIOS + " (" + Usuario.CAMPO_ID + ")); ";
    public static final String DROPEAR_TABLA_DIRECCIONES = "DROP TABLE IF EXISTS " + TABLA_DIRECCIONES;
}
