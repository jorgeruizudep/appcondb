package udep.ing.poo.appcondb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import udep.ing.poo.appcondb.db.Conexion;
import udep.ing.poo.appcondb.db.Direccion;
import udep.ing.poo.appcondb.db.Usuario;

public class MainActivity4 extends AppCompatActivity implements View.OnClickListener {

    EditText editNombreUsuario, editCalleDireccion, editNroDireccion;
    Button botonGuardar;

    Conexion conexion;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario)extras.getSerializable("usuario");

        editNombreUsuario = (EditText)findViewById(R.id.editNombre1);
        editCalleDireccion = (EditText)findViewById(R.id.editCalleDireccion);
        editNroDireccion = (EditText)findViewById(R.id.editNroDireccion);

        editNombreUsuario.setText(usuario.getNombre());
        editNombreUsuario.setEnabled(false);

        botonGuardar = (Button)findViewById(R.id.button6);
        botonGuardar.setOnClickListener(this);

        conexion = new Conexion(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == botonGuardar.getId()) {

            String calle = editCalleDireccion.getText().toString();
            String numero = editNroDireccion.getText().toString();

            if (!calle.equals("") && !numero.equals("")) {

                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Direccion.CAMPO_CALLE,calle);
                values.put(Direccion.CAMPO_NUMERO,numero);
                values.put(Direccion.CAMPO_USUARIO_FK,usuario.getId());

                long insert = db.insert(Direccion.TABLA_DIRECCIONES, null, values);
                Toast.makeText(this, "id:" + insert, Toast.LENGTH_SHORT).show();

                if (insert>-1) {

                    Intent i = new Intent(this, MainActivity3.class);
                    Bundle extras = new Bundle();
                    extras.putSerializable("usuario",usuario);
                    i.putExtras(extras);
                    startActivity(i);

                }

            }


        }

    }
}