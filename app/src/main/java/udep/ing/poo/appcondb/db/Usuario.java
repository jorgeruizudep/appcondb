package udep.ing.poo.appcondb.db;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Usuario implements Serializable {

    private int id;
    private String nombre;
    private String telefono;
    private transient Bitmap imagen; // la imagen no se tomará en cuenta en la serialización

    public Usuario(int id, String nombre, String telefono, Bitmap imagen) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.imagen = imagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public static final String TABLA_USUARIOS = "usuarios";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_TELEFONO = "telefono";
    public static final String CAMPO_FOTO = "foto";
    public static final String CAMPO_CV = "cv";

    public static final String CREAR_TABLA_USUARIOS = "CREATE TABLE " + TABLA_USUARIOS + " (" + CAMPO_ID + " INTEGER NOT NULL PRIMARY KEY, " + CAMPO_NOMBRE + " TEXT, " + CAMPO_TELEFONO + " TEXT, " + CAMPO_FOTO + " BLOB, " + CAMPO_CV + " BLOB)";
    public static final String DROPEAR_TABLA_USUARIOS = "DROP TABLE IF EXISTS " + TABLA_USUARIOS;
}
